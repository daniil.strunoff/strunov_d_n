#include <iostream>
#include <opencv2/opencv.hpp>
#include "opencv2/core.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/xfeatures2d/nonfree.hpp"
#include <opencv2/line_descriptor.hpp>
#include <opencv2/opencv_modules.hpp>
#include <iostream>
#include <vector>
#include <stdio.h>
#include <filesystem>
#include <regex>
using namespace cv;
using namespace std;
using namespace cv::xfeatures2d;
using namespace cv::line_descriptor;
namespace fs = std::experimental::filesystem;

void detect(Mat src, string path_true, string path_pred, string path_pred_img, int thresh)
{
    Mat dst, cdst, cdstP;

    ifstream inFile;
    inFile.open(path_true);

    vector<double> Numbers;
    vector<Vec4i> Truth_vec;
    double S;
    while (inFile >> S) {
        Numbers.push_back(S);
    }

    int x1, y1, x2, y2;
    Vec4i vec;
    for (auto iter = Numbers.begin(); iter != Numbers.end(); iter++) {
        x1 = *iter;
        iter++;
        y1 = *iter;
        iter++;
        x2 = *iter;
        iter++;
        y2 = *iter;
        vec = Vec4i(x1, y1, x2, y2);
        Truth_vec.push_back(vec);
    }

    Mat src_gray;

    cvtColor(src, src_gray, COLOR_BGR2GRAY);
    //imshow("gray", src_gray);

    Mat binary(src.rows, src.cols, CV_8UC1);
    inRange(src, Scalar(0, 0, 0), Scalar(thresh, thresh, thresh), binary);

    //imshow("binary", binary);

    dilate(binary, binary, Mat());
    //imshow("binary_dil", binary);

    Canny(binary, dst, 50, 200, 3);

    cvtColor(dst, cdstP, COLOR_GRAY2BGR);


    vector<Vec4i> linesP;
    HoughLinesP(dst, linesP, 1, CV_PI / 180, 15, 1, 15);


    Mat res = Mat::zeros(src_gray.size(), CV_8UC3);
    Mat truth_mat = Mat::zeros(src_gray.size(), CV_8UC3);


    for (size_t i = 0; i < linesP.size(); i++)
    {
        Vec4i l = linesP[i];
        line(cdstP, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0, 0, 255), 1, LINE_AA);
        line(res, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(181, 113, 255), 7, LINE_AA);
    }
    for (auto iter = Truth_vec.begin(); iter != Truth_vec.end(); iter++) {
        Vec4i l = *iter;
        line(truth_mat, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(255, 127, 138), 7, LINE_AA);
    }
    ofstream myfile;
    myfile.open(path_pred, std::ofstream::out | std::ofstream::trunc);
    for (auto iter = linesP.begin(); iter != linesP.end(); iter++) {
        Vec4i l = *iter;
        myfile << l[0] << " " << l[1] << " " << l[2] << " " << l[3] << "\n";
    }
    myfile.close();
    imwrite(path_pred_img, cdstP);
    //imshow("res", res);
}

vector<float> eval(int thickness, string path_true, string path_pred, string path_diff_img, Mat src)
{
    vector<float> q;
    Mat rect_t = Mat::zeros(src.size(), CV_8UC3);
    Mat rect_p = Mat::zeros(src.size(), CV_8UC3);

    ifstream inFile;
    inFile.open(path_true);

    vector<double> Numbers;
    vector<Vec4i> Truth_vec;
    double S;
    while (inFile >> S) {
        Numbers.push_back(S);
    }

    int x1, y1, x2, y2;
    Vec4i vec;
    for (auto iter = Numbers.begin(); iter != Numbers.end(); iter++) {
        x1 = *iter;
        iter++;
        y1 = *iter;
        iter++;
        x2 = *iter;
        iter++;
        y2 = *iter;
        vec = Vec4i(x1, y1, x2, y2);
        Truth_vec.push_back(vec);
    }
    inFile.close();

    
    inFile.open(path_pred);

    vector<double> Numbers_;
    vector<Vec4i> Pred_vec;
    while (inFile >> S) {
        Numbers_.push_back(S);
    }

    for (auto iter = Numbers_.begin(); iter != Numbers_.end(); iter++) {
        x1 = *iter;
        iter++;
        y1 = *iter;
        iter++;
        x2 = *iter;
        iter++;
        y2 = *iter;
        vec = Vec4i(x1, y1, x2, y2);
        Pred_vec.push_back(vec);
    }

    for (auto iter = Truth_vec.begin(); iter != Truth_vec.end(); iter++) {
        Vec4i l = *iter;
        line(rect_t, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(181, 113, 255), thickness, LINE_AA);
    }
    for (auto iter = Pred_vec.begin(); iter != Pred_vec.end(); iter++) {
        Vec4i l = *iter;
        line(rect_p, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(255, 127, 138), thickness, LINE_AA);
    }

    Mat diff = (rect_t + rect_p) / 2;
    Mat diff_f;
    diff.convertTo(diff_f, CV_32F, 1 / 255.0);
  
    float eps = 2;
    int TP = 0, FP = 0, FN = 0, TN = 0;
    for (int row = 0; row < diff.size().height; row++) {
        for (int col = 0; col < diff.size().width; col++) {
            Vec3b w = diff.at<uchar>(row, col);
            if (abs(w.val[0] - 181. / 2.) < eps) {
                FP++;
            }
            else if (abs(w.val[0] - 255. / 2.) < eps) {
                FN++;
            }
            else if (abs(w.val[0] - (255 + 181) / 2) < eps) {
                TP++;
            }
            else
                TN++;
        }
    }

    float Pr = (float)TP / (TP + FP);
    float Re = (float)TP / (TP + FN);
    float F1 = 2. / ((1 / Pr) + (1 / Re));
    q.push_back(Pr);
    q.push_back(Re);
    q.push_back(F1);
    cout << "TP: " << TP << " TN: " << TN << " FP: " << FP << " FN: " << FN << endl;
    cout << "Precision: " << Pr << " Recall: " << Re << " F1: " << F1 << endl;
    //imshow("diff", diff);
    imwrite(path_diff_img, diff);
    return q;
}

int main() {

    vector<string> str_vec;
    smatch sm;
    string path = "..\\line_detection";
    for (const auto & entry : fs::directory_iterator(path)) {
        auto str = entry.path().string();
        std::regex e ("(.*)Ground_truth(.*)\.txt");
        auto a = regex_match(str, sm, e);
        if (a) {
            auto g = sm[2].operator std::string();
            str_vec.push_back(g);
        }
    }
       
    Mat src;
    vector<float> q0;
    vector<vector<float>> q;
    for (auto iter = str_vec.begin(); iter != str_vec.end(); iter++) {
        auto s = *iter;
        Mat src = imread("plan" + s + ".png");
        detect(src, "Ground_truth" + s + ".txt", "Predicted" + s + ".txt", "pred" + s + ".png", 80);
        q0 = eval(7, "Ground_truth" + s + ".txt", "Predicted" + s + ".txt", "diff" + s + ".png", src);
        q.push_back(q0);
    }
    float av_Pr = 0;
    float av_Pe = 0;
    float av_F1 = 0;
    for (auto iteri = q.begin(); iteri != q.end(); iteri++) {
        av_Pr += (*iteri)[0];
        av_Pe += (*iteri)[1];
        av_F1 += (*iteri)[2];
    }

    av_Pr = av_Pr / q.size();
    av_Pe = av_Pe / q.size();
    av_F1 = av_F1 / q.size();

    cout << endl;
    cout << "av_Pr: " << av_Pr << " av_Re: " << av_Pe << " av_F1: " << av_F1 << " " << endl;

    int x;
    cin >> x;
    waitKey(0);
    return 0;
}